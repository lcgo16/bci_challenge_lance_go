"""
Difficulty: easy
"""

from selenium import webdriver # Not used but I just left it here since this was already here before I started.
from bs4 import BeautifulSoup
import requests
from datetime import datetime 

url = 'https://www.emservices.com.sg/tenders/'

# Preparing the CSV output file.
import csv  
header = ['advert_date','closing_dates','client','description','eligibility','link'] # Manually coded the headers for the CSV since it's static.
with open('challenge_1_output.csv', 'w', newline='', encoding='UTF8') as f: # Opens a CSV to be the output file.
    writer = csv.writer(f) 
    writer.writerow(header) # Writes the headers onto the output CSV.

    # Retrieve the HTML of the Website.
    htmlContent = requests.get(url).text
    
    # Parse the Retrieved HTML.
    soup = BeautifulSoup(htmlContent, 'lxml') # Parsing using 'lxml'. Google says that 'lxml' is a good parser.
    
    # Selecting the <tbody> element that houses the entire table using the contents in one of the cells of the table.
    tbody = soup.find(text='PASIR RIS-PUNGGOL TOWN COUNCIL').parent.parent.parent # Finding the element that holds the table by pedalling backwards from a known cell.
    tbodyContentList = tbody.children # Stores all the rows retrieved from the element housing the table.
    
    # Placing rows of the table into a list for easy access (first row on site can be accessed with trList[1]).
    trList = [] # This is a list containing the different rows in the table, or in the HTML, the <tr> tags.
    for tr in tbodyContentList: # 
        trList.append(tr)
    
    # Declaring some variables.
    condIndex = 0 # This is an index that I use to differentiate the action to be taken for different cells within a row.
    iterator = 0 # This is what I use to iterate through the different rows in the table.
    writeList = [] # This list holds temporarily the data to be written on to the csv file for one row.
    
    # Recording each cell data for each row onto the output CSV file.
    while (iterator <= 9): # This is set at 9 since the example csv displayed 10 items. It's 9 instead of 10 due to the index starting with 0.
        iterator += 1 # This iterator is placed her to skip trList[0] which isn't an actual row, but just metadata for the list.
        for td in trList[iterator]: # Loops through the different rows in the list trList.
            condIndex += 1 # Increments condIndex to keep track of which cell in the row is being worked on.
            print(condIndex, ": ", td.text) # Just for debugging - gives the console something to output. :)
            if (condIndex == 1 or condIndex == 2): # For the first two cells, both have a date string that need to be parsed, so I grouped them together.
                date = datetime.strptime(td.text, "%d/%m/%Y") # Creates a datetime object of the date string.
                writeList.append(date) # Adds created datetime object in the right format to the writeList since it's ready to be written on the CSV file.
            if (condIndex == 3 or condIndex == 4 or condIndex == 5): # For cells 3, 4, 5 - there's no special action needed, so I grouped them together.
                writeList.append(td.text) # Adds to writeList since text data is ready to write to CSV.
            if (condIndex == 6): # For cell 6, it did not need the word 'Download' in the CSV, but rather the link. This part extracts the link.
                for link in td.find_all('a', href=True): # Looks for all instances of <a> in the current cell. Would return all <a> tags in the cell.
                    # print(link['href']) # Just for debugging too.
                    writeList.append(link['href']) # For all <a> elements found, takes the link from the 'href' tag, which is added to the writeList for the CSV.
                condIndex = 0 # Resets the condIndex to 0 since the code is ready to move onto the next row cell 1.
                break # Break. 
        writer.writerow(writeList) # Writes current row data onto the output CSV.
        writeList.clear() # Clears the writeList since it'll be reused when reading the next row.
    


